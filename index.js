// console.log("Hello World");

// [SECTION] Arithmetic Operators 
	// allows us to perform mathematical operations between operands (values on either sides of the operatos).
	// it returns a numerical value.

	let x = 100;
	let y = 25;

	let sum = x + y;
	console.log("Result of addition operator: " + sum);

	let difference = x - y;
	console.log("Result of subtraction operator: " + difference);

	let product = x * y;
	console.log("Result of multplication operator: " + product);

	let quotient = x / y;
	console.log("Result of division operator: " + quotient);

	let remainder = x % y;
	console.log("Result of modulo operator: " + remainder);

	//Assignment Operator
	 //Assigns the value of the right operand to a variable

	// Basic assignment operator
	let assignmentNumber = 8;
	console.log(assignmentNumber);
	
	// Addition Assignment Operator (+=)
		// Long method
		// assignmentNumber = assignmentNumber+=2;

		// shorthand
		assignmentNumber +=2;
		console.log("Result of addition assignment operator: "+assignmentNumber);

		// Subtraction/Multiplication/Division Assignment Operator
		// (-=, *=, /=)

		assignmentNumber -=2;
		console.log("Result of addition assignment operator: "+assignmentNumber);

		assignmentNumber *=2;
		console.log("Result of addition assignment operator: "+assignmentNumber);

		assignmentNumber /=2;
		console.log("Result of addition assignment operator: "+assignmentNumber);

		assignmentNumber %=2;
		console.log("Result of addition assignment operator: "+assignmentNumber);

	// PEMDAS (Order of operations)

		let mdas = 1 + 2 - 3 * 4 / 5;
		console.log(mdas);

		// The order operation can be changed by adding parenthesis 

		let pemdas = 1 + (2 - 3) * (4 / 5);
		console.log(pemdas);

		pemdas = (1 + (2 - 3)) * (4 / 5);
		console.log(pemdas);

// [SECTION] Increment and Decrement Operator
	// This operators add or subtract values by 1 and reassign the value of the variables where the increment/decrement was applied.

		let z = 1;
		// increment (++)
		let increment = ++z;

		// the value of "z" is added by one before storing it in the increment variable
		console.log	("Result of the pre-increment: " + increment);
		//pre-increment both increment and z variable have value 2
		console.log	("Result of the pre-increment for z :" + z);

		//The value of "z" is stred in the increm
		increment = z++;
		console.log	("Result of the post-increment: " + increment);
		console.log	("Result of the post-increment for z :" + z);

		// decrement (--)
		let decrement = --z;
		console.log("Result of pre-decrement "+decrement);
		console.log("Result of pre-decrement for z "+z);

		decrement = z--;
		console.log("Result of pre-decrement "+decrement);
		console.log("Result of pre-decrement for z "+z);

// [SECTION] Type Coercion
	//automatic or implicit conversion of value from one data type to another.
	//This happens when operations are performed in different types that would normally not be po0ssible and yield irregular results.
	//"automatic conversion"

		let numA = '10';
		let numB = 12;

		let coercion = numA + numB;
		// Performing addition operation to a number and string variable will result to concatenation.
		console.log(coercion);
		console.log(typeof coercion);

		let numC = 16;
		let numD = 14;

		let nonCoercion = numC + numD;
		console.log(nonCoercion);
		console.log(typeof nonCoercion);
		/*
			- The boolean true is associated with the value of 1.
			- The boolean false is also associated with the value of 0.
		*/

		let numE = true + 1;
		console.log(numE);

		let numF = false + 1;
		console.log(numF);

//[SECTION] Comparison Operator
	// are used to evaluate and compare the left and right operands.
	// it returns a Boolean value.

		let juan = 'juan';
	// Equality Operator (==)
		/*
			-Checks wheather the operand are equal or have the same content.
			-Attempts to CONVERT AND COMPARE operands of different data types. (type coercion)

		*/
		console.log("EQUALITY OPERATOR");
		console.log(1==1);// true
		console.log(1==2);// false
		console.log(1=='1');// true
		console.log(false==0); // true
		console.log('juan'=="juan")//true
		console.log('juan'=="Juan")//false

	// Inequality Operator(!=)
		/*
			- check whether the operands are not equal/ have different data types.
			- Attempts to CONVETS AND COMPARE operands of different data types
		*/
		console.log("INEQUALITY OPERATOR");
		console.log(1!=1);// false
		console.log(1!=2);// true
		console.log(1!='1');// false
		console.log(false!=0); // false
		console.log('juan'!="juan")//false
		console.log('juan'!="Juan")//true

	// Strict Equality Operator(===)
		/*
			- chcek whether the operands are equal/have the same content.
			- also compares the data type of 2 values.
		*/
		console.log("Strictly Equality Operator:");
		console.log(1 === 1); //true
		console.log(1 === 2); //false
		console.log(1 === '1'); //false
		console.log(false === 0); //false
		console.log('juan' === "juan");//true
		console.log('juan' === "Juan");//false

	// Strict Inequality Operator(!==)
		/*
			-checks whether the operands are not equal or have the different content.
			-compares the data types of two values.
		*/

		console.log("Strict Inequality Operator:");
		console.log(1 !== 1); //false
		console.log(1 !== 2); //true
		console.log(1 !== '1'); //true
		console.log(false !== 0); //true
		console.log('juan' !== "juan");//false
		console.log('juan' !== "Juan");//true

//[SECTION] Greater Than and Less Than Operator
	//Some comparisons operators check whether one value is greater or less than to the other value.
	// Returns a Boolean value

	let a = 50;
	let b = 65;

	console.log("Greater Than and Less Than Operator");
	// GT or Greather than (>)
	let isGreaterThan = a > b;
	console.log(isGreaterThan);

	//LT ot (<)
	let isLessThan = a < b;
	console.log(isLessThan);

	// GTE or Greater Than or Equal (>=)
	let isGTorEqual = a>= b;
	console.log(isGTorEqual);

	// LTE or Less Than or Equal (<=)
	let isLTorEqual = a<=b;
	console.log(isLTorEqual);

	let numStr = "30"
	console.log(a > numStr);

	let strNum = "twenty"
	console.log(b > strNum); //what ever operator is used it will result to false, since the string is not numeric. This is usually result to NaN(Not a Number)

// [SECTION] Logical Operators
	//allows for a more specific logical combination of combinations.
	//returns a boolean value

	let isLegalAge = false;
	let isRegistered = true;

	// Logical AND operator (&& - Double Ampersand)
		//returns TRUE if all operands are true.

	let allRequirementsMet = isLegalAge && isRegistered;
	console.log("Result of logical AND operators " + allRequirementsMet);


	// Logical OR Operator (|| - Double Pipe)
		// Returns TRUE if one of the operands are TRUE.
	let someRequirementsMet = isLegalAge || isRegistered;
	console.log("Result of logical OR operators " + someRequirementsMet)

	// Logical NOT Operator (! - Exclamation Point)
		// Returns the opposite value
	console.log("Result of logical NOT Operator: " + !isRegistered);
	console.log("Result of logical NOT Operator: " + !isLegalAge);



